# Copy the following into your code to generate .ics calendar files
# one per room, that you can open in Windows Calendar or Google Calendar or ...


# imports, above the Scheduler class
from icalendar import Calendar, Event
from datetime import datetime

# --------- Add to the Scheduler class
 def toIcalEventByClassTimeRoom(self,classID,timeID,roomID):
        className  = s.classInfoByIndex[classID][0]
        instructor = s.classInfoByIndex[classID][1]
        time       = s.timeSlotByIndex[timeID][0]
        days       = s.timeSlotByIndex[timeID][2]
        room       = s.roomByIndex[roomID]

        # parameters for generating an iCalendar schedule
        # TODO: make these parameters
        SYEAR  = 2017   # events will start on this week, give the day as Monday
        SMONTH = 2
        SDAY   = 13

        # Assume time is in HHMM-HHMM format always
        (startTime,endTime) =time.split('-')
        startTimeHH = int(startTime[0:2])    # int('09') returns 9
        startTimeMM = int(startTime[2:])
        endTimeHH   = int(endTime[0:2])
        endTimeMM   = int(endTime[2:])

        dtstamp = datetime.now()
        event = Event()
        event.add('summary','{} {}'.format(className,instructor))
        event.add('dtstamp', dtstamp)
        if days == 'MTWR':
            # SDAY is monday so no change
            event.add('rrule', {'freq':'daily', 'interval':1, 'count':4})
        elif days == 'MW':
            # SDAY also starts on monday
            event.add('rrule', {'freq':'daily', 'interval':2, 'count':2})
        elif days == 'TR':
            # first class is Tuesday, one after Monday
            SDAY = SDAY + 1
            event.add('rrule', {'freq':'daily', 'interval':2, 'count':2})

        event.add('dtstart',datetime(SYEAR,SMONTH,SDAY,startTimeHH,startTimeMM))
        event.add('dtend',  datetime(SYEAR,SMONTH,SDAY,endTimeHH,endTimeMM))

        return event

    def printSchedule(self,sked):
        # Print the entire schedule
        for i in range(sked.shape[0]):
            print(self.toStringByClassTimeRoom(sked[i,0],sked[i,1],sked[i,2]))



# ---------- And anywhere else, add to generate calendar files from a schedule
# and then invoke createCalendarFiles
def displayCal(cal):
    print(cal.to_ical().replace(b'\r\n',b'\n').strip().decode('utf-8'))

def createCalendarFiles(s,sked):
    """s = scheduler object
    sked = a schedule (numpy array Numclasses x 3)
    Will write one file (in the current directory) for each
    room.
    """
    # Create iCalendar files from the schedule, one per room
    cals = {}
    for (roomID,roomStr) in s.roomByIndex.items():
        cals[roomStr] = Calendar()
        cals[roomStr].add('prodid','-//WOUScheduler//CS 431//EN')
        cals[roomStr].add('version','2.0')

    for i in range(sked.shape[0]):
        event = s.toIcalEventByClassTimeRoom(sked[i,0],sked[i,1],sked[i,2])
        roomID = sked[i,2]
        roomStr = s.roomByIndex[roomID]
        cals[roomStr].add_component(event)

    for (roomStr,cal) in cals.items():
        fname = roomStr.replace(" ","_")+".ics"
        with open(fname,'wb') as fout:
            fout.write(cal.to_ical())
